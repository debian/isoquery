# Changelog

All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.3.4] – 2024-04-27

### Added
- English translation of manpage with proper quotes.

### Changed
- Portuguese translation of manpage.

### Fixed
- Use en_GB.UTF-8 locale for testing, not C.UTF-8. Otherwise,
  the output will not get translated.
- Fix expected test output after iso-codes update


## [3.3.3] – 2023-02-28

### Added
- Turkish translation of manpage.
- Turkish translation of program.
- Spanish translation of manpage.
- Spanish translation of program.


## [3.3.2] – 2022-10-27

### Changed
- Remove unnecessary country component from Norwegian Bokmal


## [3.3.1] – 2022-10-27

### Added
- Use Weblate as translation source for program and manpage.
- Norwegian Bokmål translation of manpage.
- Norwegian Bokmål translation of program.
- Swedish translation of manpage.
- Swedish translation of program.

### Changed
- Use positional format specifiers in gettext strings to allow for
  reordering by translators.
- Switch from rst to asciidoctor for manpage generation
- Translation updates for Italian, German, French, Sinhala, Czech,
  Portuguese
- Move project to codeberg.org


## [3.3.0] – 2022-06-13

### Added
- Support country flags using Unicode "REGIONAL INDICATOR SYMBOL LETTER".
  Fixes #5

### Changed
- Update German translation of program.
- Update German translation of manpage.

### Fixed
- Reorder object files, to support building on non-Linux systems that do
  not support dynamic library runtime loader reference fixups. Fixes #7
- Fix expected test output after iso-codes update


## [3.2.7] – 2022-02-19

### Changed
- Update autotools files.

### Fixed
- Use only one equal sign in test command for POSIX conformance. Fixes #6


## [3.2.6] – 2021-11-15

### Changed
- Update autotools files.

### Fixed
- Update test data to current iso-codes 4.8.0, so that tests no longer
  fail. Closes: #998587


## [3.2.5] – 2021-08-26

### Changed
- Update autotools files.

### Fixed
- Update test to match French translation change in iso-codes
  Closes: #991653


## [3.2.4] – 2020-12-27

### Added
- Add Italian translation of program and manpage. Thanks to
  Sebastiano Pistore <SebastianoPistore.info@protonmail.ch>!

### Fixed
- Update test to match French translation change in iso-codes
  Closes: #963371


## [3.2.3] – 2018-08-18

### Fixed
- Fix FTBFS due to changed po4a string extraction in rst manpage
  Closes: #906475


## [3.2.2] – 2017-09-26

### Fixed
- Fix FTBFS due to missing newline character in German translation.
  Closes: #876855


## [3.2.1] – 2016-08-30

### Fixed
- Use Feature Test Macro for execvpe()


## [3.2.0] – 2016-08-30

### Changed
- Set up a defined environment to enable reproducible testing
- Update expected test result to match latest changes in iso-codes


## [3.1.0] – 2016-07-13

### Changed
- Updated translations:
  - Swedish, thanks to Martin Bagge / brother
  - Portuguese, thanks to Américo Monteiro
  - Vietnamese, thanks to Trần Ngọc Quân
  - Russian, thanks to Yuri Kozlov
  - Danish, thanks to Joe Hansen
  - French, thanks to Jean-Baka Domelevo Entfellner
- Updated manpage translations:
  - Portuguese, thanks to Américo Monteiro
  - French, thanks to Baptiste Jammet


## [3.0.1] – 2016-06-10

### Fixed
- Fix uninitialized variable


## [3.0.0] – 2016-06-09

### Added
- Supports the newly added ISO 3166-3 codes

### Changed
- Complete rewrite in C, using GLib
- Updated translations:
  - German, thanks to Dr. Tobias Quathamer
- Updated manpage translations:
  - German, thanks to Dr. Tobias Quathamer


## [2.0] – 2014-05-30

- Complete rewrite in Vala, significantly faster than
  previous Python implementation
- Uses libisocodes to access iso-codes XML data
- Supports the newly added ISO 639-5 codes
- Updated translations:
  - Danish, thanks to Joe Hansen
  - Russian, thanks to Yuri Kozlov
  - French, thanks to Christian Perrier
  - Portuguese, thanks to Américo Monteiro
  - German, thanks to Tobias Quathamer
  - Swedish, thanks to Martin Bagge
- Updated manpage translations:
  - German, thanks to Tobias Quathamer
  - French, thanks to David Prévot
  - Portuguese, thanks to Américo Monteiro


## [1.7] – 2012-01-23

- Correct installation path for translated manpages.


## [1.6] – 2012-01-08

- Fix a UnicodeEncodeError in version output, thanks to David Prévot.
  Closes: #650746
- Use standard distutils setup.py file for building and installing
  the package, remove waf build system completely
- Updated translations:
  - Sinhala, thanks to Danishka Navin <danishka@gmail.com>.


## [1.5] – 2011-06-14

- If the locale does not exist for the given ISO standard, print a warning
  message and use the untranslated (thus English) strings. Closes: #624147
- Review isoquery manpage, thanks to David Prévot. Closes: #627577
- New manpage translation:
  - French, thanks to David Prévot. Closes: #628841
- Update test suite to pass with current iso-codes (3.26)
- Updated translations:
  - German, thanks to Tobias Quathamer.
  - Portuguese, thanks to Américo Monteiro.
  - Swedish, thanks to Martin Bagge. Closes: #628863
  - Russian, thanks to Yuri Kozlov. Closes: #628966
  - Czech, thanks to Michal Simunek. Closes: #629112
  - French, thanks to Christian Perrier. Closes: #629164
  - Danish, thanks to Joe Hansen. Closes: #630246


## [1.4] – 2010-10-02

- New translation:
  - Vietnamese, thanks to Clytie Siddall. Closes: #598618


## [1.3] – 2010-08-15

- New translation:
  - Portuguese, thanks to Américo Monteiro. Closes: #592431
- New manpage translation:
  - Portuguese, thanks to Américo Monteiro. Closes: #592433


## [1.2] – 2010-07-31

- Fix invalid syntax for Python 2.4. Thanks to Cristian Ionescu-Idbohrn
  for the bug report and patch. Closes: #591035


## [1.1] – 2010-07-28

- Fix errors in manpages with overlong lines
- Rewrite generation of manpages with waf build system
- French translation update, thanks to Christian Perrier. Closes: #589554
- New translations:
  - Russian, thanks to Yuri Kozlov. Closes: #589521
  - Swedish, thanks to Martin Bagge / brother <brother@bsnet.se>
  - Czech, thanks to Michal Šimůnek. Closes: #589719
  - Danish, thanks to Joe Hansen. Closes: #589861
  - Sinhala, thanks to Danishka Navin.
  - Spanish, thanks to Omar Campagne. Closes: #590564


## [1.0] – 2010-06-30

- Complete rewrite of program in Python
- Support of ISO 639-3166-and ISO-3-2


## [0.18] – 2009-06-01

- Use gettext.h instead of libintl.h, as recommended by GNU gettext
- Update test suite to pass with current iso-codes (3.10)


## [0.17] – 2008-11-26

- Change my last name to Quathamer
- Update test suite to pass with current iso-codes (3.4)


## [0.16] – 2008-07-01

- Update test suite to pass with current iso-codes (3.1)


## [0.15] – 2008-06-02

- Update man page with ISO 15924
- Fix bug that results in a crash if the environment variable
  LANGUAGE is not set
- Update test suite to pass with current iso-codes (3.0)


## [0.14] – 2008-04-24

- Fix bug which could cause isoquery to throw an Glib::ConvertError.
  Thanks to George Danchev for the bug report. Closes: #474534


## [0.13] – 2008-03-12

- Use C++ vectors for the list of possible xpaths
- New command line options "-0" and "--null" to separate the output
  with NULL characters instead of newline
- Translation updates:
  - French, thanks to Christian Perrier
  - German, thanks to Tobias Quathamer


## [0.12] – 2008-01-24

- Add #include statements required by GCC 4.3. Thanks to
  Martin Michlmayr for the bug report and patch. Closes: #462206
- Better error message if the XML input file cannot be opened
- Translation updates:
  - French, thanks to Christian Perrier
  - German, thanks to Tobias Quathamer


## [0.11] – 2008-01-09

- Require po4a in configure script for manpage translations
- Require pkg-config and libxml++ (>2.6.0) in configure script
- Add support for ISO 15924
- Update tests to pass with iso-codes 1.8
- Update French translation, thanks to Christian Perrier


## [0.10] – 2008-01-02

- Bug fix:
  The codes in ISO 639 were shown twice if 2B and 2T code are
  identical. Examples are vie, swa.
- Setup testing framework to catch regression bugs
- Remove call to bindtextdomain(), enabling translations of the
  program output itself


## [0.9] – 2007-11-13

- Add man page
- Use po4a to translate man page into German


## 0.8 – UNRELEASED

- Initial setup, using basic classes and C++ code
- Perform gettextize on source code

[Unreleased]: https://codeberg.org/toddy/isoquery/compare/v3.3.4...HEAD
[3.3.4]: https://codeberg.org/toddy/isoquery/compare/v3.3.3...v3.3.4
[3.3.3]: https://codeberg.org/toddy/isoquery/compare/v3.3.2...v3.3.3
[3.3.2]: https://codeberg.org/toddy/isoquery/compare/v3.3.1...v3.3.2
[3.3.1]: https://codeberg.org/toddy/isoquery/compare/v3.3.0...v3.3.1
[3.3.0]: https://codeberg.org/toddy/isoquery/compare/v3.2.7...v3.3.0
[3.2.7]: https://codeberg.org/toddy/isoquery/compare/v3.2.6...v3.2.7
[3.2.6]: https://codeberg.org/toddy/isoquery/compare/v3.2.5...v3.2.6
[3.2.5]: https://codeberg.org/toddy/isoquery/compare/v3.2.4...v3.2.5
[3.2.4]: https://codeberg.org/toddy/isoquery/compare/v3.2.3...v3.2.4
[3.2.3]: https://codeberg.org/toddy/isoquery/compare/v3.2.2...v3.2.3
[3.2.2]: https://codeberg.org/toddy/isoquery/compare/v3.2.1...v3.2.2
[3.2.1]: https://codeberg.org/toddy/isoquery/compare/isoquery-3.2.0...v3.2.1
[3.2.0]: https://codeberg.org/toddy/isoquery/compare/isoquery-3.1.0...isoquery-3.2.0
[3.1.0]: https://codeberg.org/toddy/isoquery/compare/isoquery-3.0.1...isoquery-3.1.0
[3.0.1]: https://codeberg.org/toddy/isoquery/compare/isoquery-3.0.0...isoquery-3.0.1
[3.0.0]: https://codeberg.org/toddy/isoquery/compare/isoquery-2.0...isoquery-3.0.0
[2.0]: https://codeberg.org/toddy/isoquery/compare/isoquery-1.7...isoquery-2.0
[1.7]: https://codeberg.org/toddy/isoquery/compare/isoquery-1.6...isoquery-1.7
[1.6]: https://codeberg.org/toddy/isoquery/compare/isoquery-1.5...isoquery-1.6
[1.5]: https://codeberg.org/toddy/isoquery/compare/isoquery-1.4...isoquery-1.5
[1.4]: https://codeberg.org/toddy/isoquery/compare/isoquery-1.3...isoquery-1.4
[1.3]: https://codeberg.org/toddy/isoquery/compare/isoquery-1.2...isoquery-1.3
[1.2]: https://codeberg.org/toddy/isoquery/compare/isoquery-1.1...isoquery-1.2
[1.1]: https://codeberg.org/toddy/isoquery/compare/isoquery-1.0...isoquery-1.1
[1.0]: https://codeberg.org/toddy/isoquery/compare/isoquery-0.18...isoquery-1.0
[0.18]: https://codeberg.org/toddy/isoquery/compare/isoquery-0.17...isoquery-0.18
[0.17]: https://codeberg.org/toddy/isoquery/compare/isoquery-0.16...isoquery-0.17
[0.16]: https://codeberg.org/toddy/isoquery/compare/isoquery-0.15...isoquery-0.16
[0.15]: https://codeberg.org/toddy/isoquery/compare/isoquery-0.14...isoquery-0.15
[0.14]: https://codeberg.org/toddy/isoquery/compare/isoquery-0.13...isoquery-0.14
[0.13]: https://codeberg.org/toddy/isoquery/compare/isoquery-0.12...isoquery-0.13
[0.12]: https://codeberg.org/toddy/isoquery/compare/isoquery-0.11...isoquery-0.12
[0.11]: https://codeberg.org/toddy/isoquery/compare/isoquery-0.10...isoquery-0.11
[0.10]: https://codeberg.org/toddy/isoquery/compare/isoquery-0.9...isoquery-0.10
[0.9]: https://codeberg.org/toddy/isoquery/releases/tag/isoquery-0.9
